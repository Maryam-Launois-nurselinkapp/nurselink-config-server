FROM adoptopenjdk/openjdk11:latest

ARG JAR_FILE=target/*.jar

RUN mkdir /opt/config-server

COPY ${JAR_FILE} /opt/config-server/app.jar

ENTRYPOINT ["java", "-jar", "/opt/config-server/app.jar"]